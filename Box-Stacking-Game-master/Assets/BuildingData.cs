using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingData 
{
    public int id;
    public bool state;
    public BuildingData(int _id, bool _state)
    {
        id = _id;
        state = _state;
    }
}
