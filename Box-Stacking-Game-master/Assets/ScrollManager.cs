﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

public class ScrollManager : Singleton<ScrollManager>,/* IDragHandler,*/ IEndDragHandler, IBeginDragHandler
{
    public Transform scrollPoint;
    public Camera scrollCam;
    private Vector3 firstScrollPos;
    public float speed;
    public GameObject gameplayCam;
    public bool canScroll;
    public Vector3 newposOnMap;
    private bool dragBegin;
    public GameObject trailHolder;
    public GameObject landmarkHolder;
    private List<Collider2D> colliders;
    Vector3 lastPos;

    // Start is called before the first frame update
    void Start()
    {
        //    colliders = trailHolder.GetComponentsInChildren<Collider2D>(true).ToList();
        //    landmarkHolder.GetComponentsInChildren<Collider2D>(true).ToList().ForEach(c =>
        //    {
        //        colliders.Add(c);
        //    });
        //    ActivateMapCam();
        //    GameManager.main.simulationManager.OnSimulationStarted.AddListener((Simulation sim) => DeactivateMapCam());
        //    GameManager.main.simulationManager.OnSimulationEnded.AddListener((Simulation sim) => ActivateMapCam());
    }

    //public void OnDrag(PointerEventData eventData)
    //{
    //    if (canScroll)
    //    {
    //        // this gets logged after about 500 milliseconds
    //        //scrollPoint.transform.position = -Input.mousePosition;
    //        Vector3 newPos = scrollCam.ScreenToWorldPoint(Input.mousePosition) - firstScrollPos;
    //        newposOnMap = -(newPos * speed);
    //        Debug.Log(Input.mousePosition);
    //        scrollPoint.transform.position += -(newPos * speed);
    //        //scrollPoint.transform.position = new Vector3(scrollPoint.transform.position.x + (Input.mousePosition.x), scrollPoint.transform.position.y + (Input.mousePosition.y), 0);
    //    }
    //}
    public void OnDragging()
    {
        if (canScroll)
        {
            // this gets logged after about 500 milliseconds
            //scrollPoint.transform.position = -Input.mousePosition;
            Vector3 newPos = scrollCam.ScreenToWorldPoint(Input.mousePosition) - firstScrollPos;
            newposOnMap = -(newPos * speed);
            //Debug.Log(Input.mousePosition);
            scrollPoint.transform.position += -(newPos * speed);
            //scrollPoint.transform.position = new Vector3(scrollPoint.transform.position.x + (Input.mousePosition.x), scrollPoint.transform.position.y + (Input.mousePosition.y), 0);
        }
    }
    public void ActivateMapCam()
    {
        canScroll = true;
        GetComponentsInChildren<ScrollItem>(true).ToList().ForEach(item => item.gameObject.SetActive(true));
        gameplayCam.SetActive(false);
    }
    public void DeactivateMapCam()
    {
        canScroll = false;
        GetComponentsInChildren<ScrollItem>().ToList().ForEach(item => item.gameObject.SetActive(false));
        gameplayCam.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            if (!dragBegin)
            {
                if (canScroll)
                {
                    lastPos = Input.mousePosition;
                    firstScrollPos = scrollCam.ScreenToWorldPoint(Input.mousePosition);
                    dragBegin = true;
                }
            }
            if (Vector3.Distance(Input.mousePosition, lastPos) > 10)
            {
                OnDragging();
                //colliders.ForEach(col=> col.enabled = false);
            }
        }
        else
        {
            dragBegin = false;
                //colliders.ForEach(col=> col.enabled = true);

        }
    }
    //void OnMouseDrag()
    //{
    //    Debug.LogError("esto pasaaa");
    //    scrollPoint.transform.position = -scrollCam.ScreenToWorldPoint(Input.mousePosition);
    //}

    public void OnEndDrag(PointerEventData eventData)
    {
        //currPos = scrollPoint.transform.position;
        //firstScrollPos = scrollCam.ScreenToWorldPoint(Input.mousePosition);
        //scrollPoint.transform.position = scrollCam.ScreenToWorldPoint(new Vector2(Screen.width / 2,Screen.height / 2));
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        if (canScroll)
        {
            firstScrollPos = scrollCam.ScreenToWorldPoint(Input.mousePosition);
        }
    }
    public void OnInitializePotentialDrag(PointerEventData ped)
    {
        ped.useDragThreshold = false;
    }
}
