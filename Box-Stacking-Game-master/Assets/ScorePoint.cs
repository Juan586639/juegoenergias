using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScorePoint : MonoBehaviour
{
    public int id;
    bool used = false;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<Module>())
        {
            if (collision.GetComponent<Module>().id == id)
            {
                if (!used)
                {
                    ScoreManager.main.score += 1;
                    used = true;
                }
            }
            else
            {
                LoseTrigger.main.hasLost = true;
            }

        }
    }
}
