using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BuildingsManager : Singleton<BuildingsManager>
{
    public List<Building> buildings;
    // Start is called before the first frame update
    void Start()
    {
    }
    public void SetBuildings()
    {
        buildings = GetComponentsInChildren<Building>().ToList();

    }
    // Update is called once per frame
    void Update()
    {

    }
    public int GetPercentage()
    {
        List<Building> successBuildings = buildings.Where(b => b.data.state).ToList();
        return buildings.Count - successBuildings.Count;
    }
    public int GetSuccessBuildings()
    {
        List<Building> successBuildings = buildings.Where(b => b.data.state).ToList();
        return successBuildings.Count();
    }
}
