using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class DataManager : Singleton<DataManager>
{
    public int currBuilding;
    public int cuantityOfModules;
    public GameObject panel;
    bool firstTime = true;
    public List<BuildingData> buildingsData = new List<BuildingData>();
    public int currentTip = 0;

    // Start is called before the first frame update
    void Start()
    {
    }
    public void Init()
    {
        BuildingsManager.main.SetBuildings();
        if (firstTime)
        {
            Debug.Log("Init " + BuildingsManager.main.buildings.Count);
            for (int i = 0; i < BuildingsManager.main.buildings.Count; i++)
            {
                buildingsData.Add(new BuildingData(i, false));

            }
            firstTime = false;
        }
        else
        {
            FirstPanel.main.gameObject.SetActive(false);
        }
    }
    private void Update()
    {

    }

    // Update is called once per frame
    public void SetCurrentBuilding(int _building)
    {
        currBuilding = _building;
    }
    public void InitBuildings()
    {
        Init();
        for (int i = 0; i < BuildingsManager.main.buildings.Count; i++)
        {
            BuildingsManager.main.buildings.Where(b => b.id == i).ToList()[0].Init(buildingsData[i]);
            //Debug.Log(i + " "+BuildingsManager.main.buildings[i].data.state);

        }
        if (((BuildingsManager.main.buildings.Count -buildingsData.Where(b => b.state).ToList().Count )* 100 / BuildingsManager.main.buildings.Count) < 75 && ((BuildingsManager.main.buildings.Count - buildingsData.Where(b => b.state).ToList().Count) * 100 / BuildingsManager.main.buildings.Count) > 50)
        {
            ProgressPopUp.main.SetText("felicidades la ciudad esta en menos del 75% de huella de carbono. *A 2030, Ecopetrol reducir� en 25% sus emisiones frente a lo emitido en 2019.");
        }
        if (((BuildingsManager.main.buildings.Count - buildingsData.Where(b => b.state).ToList().Count) * 100 / BuildingsManager.main.buildings.Count) < 50 && ((BuildingsManager.main.buildings.Count - buildingsData.Where(b => b.state).ToList().Count) * 100 / BuildingsManager.main.buildings.Count) > 25)
        {
            ProgressPopUp.main.SetText("Menos del 50% de huella de carbono. *Nuevas metas aportan a los objetivos del Acuerdo de Par�s y de Colombia de reducir las emisiones de gases efecto invernadero a 2030.");
        }
        if (((BuildingsManager.main.buildings.Count - buildingsData.Where(b => b.state).ToList().Count) * 100 / BuildingsManager.main.buildings.Count) < 25 && ((BuildingsManager.main.buildings.Count - buildingsData.Where(b => b.state).ToList().Count) * 100 / BuildingsManager.main.buildings.Count) > 0)
        {
            ProgressPopUp.main.SetText("felicidades la ciudad esta en menos del 25% de huella de carbono. *En la �ltima d�cada, Ecopetrol redujo 8,4 millones de toneladas equivalentes de CO2.");
        }
        Tips.main.ShowNextTip();
        if (buildingsData.Where(b => b.state).ToList().Count == BuildingsManager.main.buildings.Count)
        {
            ProgressPopUp.main.SetText("felicidades la ciudad esta a salvo, has ganado");
            ProgressPopUp.main.GetComponentInChildren<Button>().onClick.AddListener(() =>
            {
                firstTime = true;
                Destroy(this.gameObject);
                SceneManager.LoadScene("Demo");
            });
        }
    }
    public void SetNewBuilding()
    {
        buildingsData.Where(b => b.id == currBuilding).ToList()[0].state = true;
    }
}
