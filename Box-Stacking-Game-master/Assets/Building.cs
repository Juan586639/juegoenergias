using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class Building : MonoBehaviour
{
    public GameObject popUp;
    public BuildingData data;
    public int id;
    public GameObject emptyFX;
    public GameObject tower;
    public int difficulty;
    void Update()
    {
        // Check for mouse input

    }
    public void Interact()
    {
        popUp.SetActive(true);
        DataManager.main.currBuilding = id;
        DataManager.main.cuantityOfModules = difficulty;
    }
    public void Init(BuildingData _data)
    {
        data = _data;
        if (data.state)
        {
                Debug.Log("gano" +name);
            if (tower && emptyFX)
            {
                tower.gameObject.SetActive(true);
                emptyFX.SetActive(false);
            }
        }
    }
}