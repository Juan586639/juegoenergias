using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Linq;

public class GameManager : MonoBehaviour
{
    public int percentage;
    public Slider carbon;
    public TextMeshProUGUI textoBuildings;
    // Start is called before the first frame update
    void Start()
    {
        DataManager.main.InitBuildings();
    }

    // Update is called once per frame
    void Update()
    {
        carbon.value = BuildingsManager.main.GetPercentage();
        textoBuildings.text = BuildingsManager.main.GetSuccessBuildings() + " de " + BuildingsManager.main.buildings.Count() + " estructuras";
    }
}
