using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Tips : Singleton<Tips>
{
    public List<string> tips;
    public TextMeshProUGUI text;
    public GameObject holder;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    public void ShowNextTip()
    {
        holder.SetActive(true);
        text.text = tips[DataManager.main.currentTip];
        DataManager.main.currentTip++;
    }
}
