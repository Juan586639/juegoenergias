﻿using UnityEngine;
using System.Collections;
using TMPro;

public class ScoreManager : Singleton<ScoreManager>
{

    public int score = 0;
    public int buildingsSize;
    public GUISkin tempSkin;
    public GameObject winPopUp;
    public TextMeshProUGUI scoreText;
    public bool horizontal;

    private void Start()
    {
        buildingsSize = DataManager.main.cuantityOfModules;
    }
    void OnGUI()
    {
        //GUI.skin = tempSkin;
        //GUI.Label(new Rect(0, 0, 1000, 500), "Score: " + score.ToString());
    }
    private void Update()
    {
        if (!horizontal)
        {
            scoreText.text = score + " de " + buildingsSize + " pisos";
        }
        else
        {
            scoreText.text = score + " de " + buildingsSize + " modulos apilados";

        }
        if (score >= buildingsSize)
        {
            LoseTrigger.main.hasWon = true;
            winPopUp.SetActive(true);
            DataManager.main.SetNewBuilding();
        }
    }

}
